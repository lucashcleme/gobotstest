Teste prático - GoBots
Introdução
Pensando em acessibilidade, a GoBots está construindo um serviço para ler mensagens digitadas pelo bot e tocá-las como áudio. Como muitas vezes uma mensagem acaba sendo escrita com a Internetês (exemplo: "td bm?"), antes de tocá-la como áudio é necessário que a mensagem seja processada para "traduzir" as abreviações.
Objetivos
Criar um serviço simples que aceita chamadas RESTful, tendo um endpoint que recebe uma string de texto e retorna uma string com termos da Internetês "traduzidos" para uma escrita mais formal. Você pode utilizar qualquer linguagem que preferir (java/python/kotlin etc). 

Criar uma aplicação frontend simples que se comunica com o serviço acima, enviando uma string digitada em algum input do layout e mostrando a string processada, ou seja, o retorno do seu serviço. Você pode utilizar qualquer linguagem e framework que preferir (html, css, angular etc).

Diferencial(opcional)
Após mostrar a string processada, usar o speaker para que seja reproduzido o áudio dessa string, para isso será necessário implementar um pequeno trecho de código em javaScript veja a documentação:

Para esse projeto, você pode utilizar a API de Speech Synthesis no frontend:
https://developers.google.com/web/updates/2014/01/Web-apps-that-talk-Introduction-to-the-Speech-Synthesis-API


O que é esperado
README.md com instruções para rodar o backend e frontend 

Serviço simples com endpoint para:
Receber uma string e retornar essa string com termos da Internetês "traduzidos" para uma escrita mais formal.

Aplicação frontend contendo:
Página com formulário de envio para que uma string inserida seja processada
No retorno, deve aparecer a string processada 

IMPORTANTE: 
Não é necessário que todos os textos possíveis sejam tratados, mas seguem alguns exemplos que devem ser tratados:
"oi, td bm?" => "oi, tudo bem?"
"q dia eh hoje?" => "que dia é hoje?"
"qto custa?" => "quanto custa?"
"qdo será enviado?" => "quando será enviado?"
"obg" => "obrigado"
"vc tb" => "você também"
Diferenciais
Versionamento de código organizado
Reproduzir o áudio da mesma
Boas práticas para as linguagens/frameworks escolhidos
Interface do frontend com atenção à usabilidade
Dicas
Para testar a API RESTful criada, você poderá utilizar o POSTMAN
Caso use Java para criar sua API aconselhamos que utilize o eclipse
Caso use python para criar sua API aconselhamos que utilize o pycharm
Submissão
Caso não seja possível terminar, não tem problema, envie o código que conseguiu fazer com um comentário geral explicando quais seriam os próximos passos.
Faça um fork desse projeto e compartilhe com a pessoa responsável pela entrevista (victor@gobots.com.br e lucas.leme@gobots.com.br)
Repositório disponível publicamente no GitHub